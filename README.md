# CommonMark

## Introduction
> Markdown is a plain text format used to write structured documents. The third-party library CommonMark is used to convert the Markdown format to HTML or XML for display on web pages. This library demonstrates the use of [CommonMark](https://github.com/commonmark/commonmark.js) in OpenHarmony.

## How to Install
```
ohpm install commonmark
ohpm install @types/commonmark
```
For details about the OpenHarmony ohpm environment configuration, see [OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).

## How to Use

```
var reader = new commonmark.Parser();
var writer = new commonmark.HtmlRenderer();
var parsed = reader.parse("Hello *world*"); // parsed is a 'Node' tree
// transform parsed if you like...
var result = writer.render(parsed); // result is a String
```

## Available APIs
```
var reader = new commonmark.Parser({smart: true});
var writer = new commonmark.HtmlRenderer({sourcepos: true});
```

**Parser** currently supports the following:

**smart**: If the value is **true**, straight quotes are changed to curly ones, **--** is changed to an en dash, **---** is changed to an em dash, and **...** is changed to ellipses.

Both **HtmlRenderer** and **XmlRenderer** (see below) support the following options:

- **sourcepos**: If the value is **true**, the source position information of block-level elements is displayed in the **data-sourcepos** attribute (for HTML) or **sourcepos** attribute (for XML).

- **safe**: If the value is **true**, raw HTML will not be passed through to HTML output (it will be replaced by comments), and potentially insecure URLs in links and images (URLs beginning with **javascript:**, **vbscript:**, and **file:**, and with a few exceptions **data:**) will be replaced with empty strings.

- **softbreak**: specifies raw strings to be used for soft breaks.

- **esc**: specifies the function used to escape strings. Its argument is a string.

For example, to render soft breaks as hard breaks in HTML, use the following statement:

`var writer = new commonmark.HtmlRenderer({softbreak: "<br />"});`

To render them as space characters, use the following statement:

`var writer = new commonmark.HtmlRenderer({softbreak: " "});`

**XmlRenderer** is an alternative to **HtmlRenderer** and will generate an XML representation of the AST:

`var writer = new commonmark.XmlRenderer({sourcepos: true});`

The parser returns a node. The following public properties are defined (these marked as read-only have only a getter, not a setter):
* type (read-only): a String, one of text, softbreak, linebreak, emph, strong, html_inline, link, image, code, document, paragraph, block_quote, item, list, heading, code_block, html_block, thematic_break.
* firstChild (read-only): a Node or null.
* lastChild (read-only): a Node or null.
* next (read-only): a Node or null.
* prev (read-only): a Node or null.
* parent (read-only): a Node or null.
* sourcepos (read-only): an Array with the following form: [[startline, startcolumn], [endline, endcolumn]].
* isContainer (read-only): true if the Node can contain other Nodes as children.
* literal: the literal String content of the node or null.
* destination: link or image destination (String) or null.
* title: link or image title (String) or null.
* info: fenced code block info string (String) or null.
* level: heading level (Number).
* listType: a String, either bullet or ordered.
* listTight: true if list is tight.
* listStart: a Number, the starting number of an ordered list.
* listDelimiter: a String, either ) or . for an ordered list.
* onEnter, onExit: Strings, used only for custom_block or custom_inline.

Nodes have the following public methods:
* **appendChild(child)**: appends a child node to the end of the node's children.
* **prependChild(child)**: prepends a child node to the beginning of the node's children.
* **unlink()**: removes a node from the tree, disconnects the node from its siblings and parents, and closes up gaps as required.
* **insertAfter(sibling)**: inserts a sibling node after the node.
* **insertBefore(sibling)**: inserts a sibling node before the node.
* **walker()**: returns a NodeWalker that can be used to iterate through the node tree rooted in the node.

The NodeWalker returned by **walker()** has two methods:

- **next()**: returns an object with properties **entering** (a boolean, which is **true** when the node is entered from a parent or sibling, and **false** when it is reentered from a child); returns **null** when walking the tree is finished.

- **resumeAt(node, entering)**: resets the iterator to resume at a specified node and setting for **entering**. (Generally, this operation is not required unless a destructive update is performed on the node tree.)

Here is an example of the use of a NodeWalker to iterate through the tree, making transformations. This simple example converts the contents of all text nodes to ALL CAPS:
```
let walker = parsed.walker();
let event, node;

while ((event = walker.next())) {
  node = event.node;
  if (event.entering && node.type === 'text') {
    node.literal = node.literal.toUpperCase();
  }
}
```
This more complex example converts emphasis to ALL CAPS:
```
let walker = parsed.walker();
let event, node;
let inEmph = false;

while ((event = walker.next())) {
  node = event.node;
  if (node.type === 'emph') {
    if (event.entering) {
      inEmph = true;
    } else {
      inEmph = false;
      // add Emph node's children as siblings
      while (node.firstChild) {
        node.insertBefore(node.firstChild);
      }
      // remove the empty Emph node
      node.unlink()
    }
  } else if (inEmph && node.type === 'text') {
      node.literal = node.literal.toUpperCase();
  }
}
```

## Constraints
This project has been verified in the following version:

- DevEco Studio: 4.1 Canary (4.1.3.317)

- OpenHarmony SDK: API 11 (4.1.0.36)

## Directory Structure
````
|---- CommonMarkOHOS  
|     |---- entry  # Sample code
|     |---- README_EN.md  # Readme                   
````

## How to Contribute
If you find any problem when using CommonMark, submit an [issue](https://gitee.com/openharmony-tpc/commonmark/issues) or a [PR](https://gitee.com/openharmony-tpc/commonmark/pulls).

## License
This project is licensed under [Apache License 2.0](https://gitee.com/openharmony-tpc/commonmark/blob/master/LICENSE).
